package com.jlngls.heranca

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


open class Animal  {

   open fun dormir(){
        println("dormir")

    }
    fun correr(){

    }

}
class Cao : Animal() {

    //possso reescrever o metodo se ele estiver open

    override fun dormir(){
        println("dormir como um cao")
        // para eu usar o dormir original depois que eu o subescrevi, eu tenho que usar o super""
        super.dormir()
    }

}
// para estender, use :
class Passaro : Animal(){
}

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // AQUI FAZ APARECER O METODO SOBESCRITO
        var cao = Cao()
        cao.dormir()


        // Aqui o metodo original
        var passaro = Passaro()
        passaro.dormir()
    }


}